const {app, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const { nanovor, nanoscope } = require('./components/nanovor')

let win;

app.on('ready', main);

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        app.quit();
    }
});

app.on('activate', () => {
    if(win === null){
        main()
    }
})

//IPC's

ipcMain.on('update-local-nanovar', (event, arg) => {
    event.sender.send("update-local-nanovar", nanovor.getList(nanovor.getPath()));
});

//Custom Functions

function main(){
    let win = new BrowserWindow({
        width: 900, 
        height: 800,
        nodeIntegration: true,
        resizable: false
    });
    BrowserWindow.addDevToolsExtension("C:\\Users\\zarch\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\nhdogjmejiglipccpnnnanhbledajbpd\\4.1.5_0")
    win.setMenu(null);

    win.webContents.openDevTools();

    win.loadFile(path.resolve(__dirname, "web/index.html"));

    win.on('closed', () => {
        win = null;
    });

    nanoscope.detect();
}

