const { ipcRenderer } = require('electron')

//Vue component
var nanovorList = {
    props: {
        nanoscope: Boolean
    },
    data: function() {
        return {
            nanovors: [],
            isLoading: false
        }
    },
    methods: {
        sendUpdateList: function() {
            ipcRenderer.send(this.nanoscope ? "update-local-nanovar" : "update-nanoscope-nanovor");
        },
        updateList(list){
            this.nanovors = list;
        }
    },
    computed: {
        title() {
            return this.nanoscope ? 'Local Nanovor' : 'Nanoscope Nanovor';
        }
    },
    mounted: function() {
        this.sendUpdateList(this.nanoscope);
    },
    template: `
        <div>
            <h3 class="text-info">{{ title }}</h3>
            <div class="border rounded-lg p-1 m-0 shadow-sm" style="overflow-y:scroll;height:450px;">
                <table v-if="nanovors.length > 0" class="table table-hover">
                    <tbody>
                    <tr v-for="nanovor in nanovors">
                        <td class="font-weight-bold">{{nanovor.name}}</td>
                    </tr>
                    </tbody>
                </table>
                <div v-if="isLoading" class="spinner-border text-info"></div>
            </div>
        </div>
    `
};

//Update local Nanovor list
ipcRenderer.on("update-local-nanovar", (event, arg) => {
    nanovorList.methods.updateList(arg);
});

//Update local Nanovor list
ipcRenderer.on("update-nanoscope-nanovor", (event, arg) => {
    this.nanovors = arg;
});

module.exports = nanovorList;