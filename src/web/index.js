const { remote, ipcRenderer } = require('electron')
const NanovorList = require("./components/nanovorList")

//VUE
var app = new Vue({
    el: '#app',
    data: {
    },
    components: {
        'nanovor-list': NanovorList
    }
});



//Devtool stuff
document.addEventListener("keydown", function (e) {
    if (e.which === 123) {
        remote.getCurrentWindow().toggleDevTools();
    } else if (e.which === 116) {
        location.reload();
    }
});