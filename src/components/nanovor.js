const fs = require('fs');
const drivelist = require('drivelist');
const path = require('path')

localNanovorPath = path.resolve(__dirname, "../../localNanovor/");
nanoscopePath = "";

var nanovor = {
    getList: () => {
        return searchPath(nanovor.getPath());
    },
    getPath: () => {
        return localNanovorPath;
    }
}

var nanoscope = {
    detect: async () => {
        console.log("Searching for nanosope...");
        let drives = await drivelist.list();
        
        drives.forEach(drive => {
            if(!drive.isSystem && drive.mountpoints.length > 0 && drive.description === "Nanovor Nanoscope Device USB Device"){
                nanoscopePath = drive.mountpoints[0].path;
                console.log("Found Nanoscope on drive: " + nanoscopePath);
            }
        });
        
        if(nanoscopePath === ""){
            console.log("No Nanoscope found!");
        }
    },
    isConnected: () => {
        if(nanoscope.getPath() != ""){
            return fs.existsSync(path.resolve(nanoscopePath, "plyinfo.bbb"));
        }

        return false;
    },
    getPath: () => {
        return nanoscopePath;
    },
    getList: () => {
        return searchPath(nanoscope.getPath());
    },
    moveNanovor: (path) => {

    },
    removeNanovor: (index) => {
        
    }
}

function searchPath(path){
    console.log("Indexing Nanovor at path " + path);
    path += "/"

    var nanovorTmp = [];

    fs.readdirSync(path).forEach( (file) => {
        if(fs.statSync(path + file).isDirectory() && fs.existsSync(path + file + "/vinfo.bbb")){
            var data = fs.readFileSync(path + file + "/vinfo.bbb", "ascii");

            nanovorTmp.push({
                name: data.substr(154, 128).cleanUp(),
                attacks: [
                    {
                        name: data.substr(478, 128).cleanUp(),
                        description: data.substr(606, 128).cleanUp()
                    },
                    {
                        name: data.substr(1409, 128).cleanUp(),
                        description: data.substr(1538, 128).cleanUp()
                    },
                    {
                        name: data.substr(2341, 128).cleanUp(),
                        description: data.substr(2469, 128).cleanUp()
                    }
                ]
            });
        }
    });

    console.log("Indexed Nanovor!")
    return nanovorTmp;
}

String.prototype.cleanUp = function() {
    return this.replace(new RegExp("\u0000", 'g'), "");
}

module.exports = { nanovor, nanoscope }